class CreatePlayedAnswers < ActiveRecord::Migration
  def change
    create_table :played_answers do |t|
      t.references :played_question, index: true, foreign_key: true
      t.references :answer, index: true, foreign_key: true
      t.references :message, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
