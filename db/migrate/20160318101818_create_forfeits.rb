class CreateForfeits < ActiveRecord::Migration
  def change
    create_table :forfeits do |t|
      t.text :title
      t.integer :cost

      t.timestamps null: false
    end
  end
end
