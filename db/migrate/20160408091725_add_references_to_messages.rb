class AddReferencesToMessages < ActiveRecord::Migration
  def change
    add_reference :messages, :question, index: true, foreign_key: true
    add_reference :messages, :answer, index: true, foreign_key: true
  end
end
