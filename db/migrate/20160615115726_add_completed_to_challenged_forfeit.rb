class AddCompletedToChallengedForfeit < ActiveRecord::Migration
  def change
    add_column :challenged_forfeits, :completed, :boolean, default: false
  end
end
