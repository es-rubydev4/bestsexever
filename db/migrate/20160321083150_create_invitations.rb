class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.boolean :accepted, default: false
      t.integer :sender_id
      t.integer :recipient_id
      t.references :conversation, index: true, foreign_key: true


      t.timestamps null: false
    end

    add_index :invitations, :sender_id
    add_index :invitations, :recipient_id
  end
end
