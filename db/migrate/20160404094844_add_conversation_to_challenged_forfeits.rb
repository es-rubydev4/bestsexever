class AddConversationToChallengedForfeits < ActiveRecord::Migration
  def change
    add_reference :challenged_forfeits, :conversation, index: true, foreign_key: true
  end
end
