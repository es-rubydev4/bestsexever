class CreateChallengedForfeits < ActiveRecord::Migration
  def change
    create_table :challenged_forfeits do |t|
      t.references :forfeit, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.integer :recipient_id

      t.timestamps null: false
    end

    # add_index :challenged_forfeits, :sender_id
    add_index :challenged_forfeits, :recipient_id
  end
end
