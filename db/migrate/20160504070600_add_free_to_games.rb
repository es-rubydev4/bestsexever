class AddFreeToGames < ActiveRecord::Migration
  def change
    add_column :games, :free, :boolean, default: false
  end
end
