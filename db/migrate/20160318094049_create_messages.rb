class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      # t.references :user, index: true, foreign_key: true
      t.references :conversation, index: true, foreign_key: true
      t.boolean :user2_read, default: false
      t.boolean :user1_read, default: false
      t.integer :winner_id
      t.integer :loser_id
      t.integer :user1_id
      t.integer :user2_id
      t.boolean :rps
      t.boolean :answered


      t.timestamps null: false
    end

    add_index :messages, :winner_id
    add_index :messages, :loser_id
    add_index :messages, :user1_id
    add_index :messages, :user2_id
  end
end
