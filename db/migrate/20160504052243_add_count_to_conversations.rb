class AddCountToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :count, :integer, default: 0
  end
end
