class AddGenderToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :male, :boolean
    add_column :questions, :female, :boolean
  end
end
