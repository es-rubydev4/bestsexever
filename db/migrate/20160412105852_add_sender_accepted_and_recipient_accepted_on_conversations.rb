class AddSenderAcceptedAndRecipientAcceptedOnConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :sender_accepted, :boolean, default: false
    add_column :conversations, :recipient_accepted, :boolean, default: false
  end
end
