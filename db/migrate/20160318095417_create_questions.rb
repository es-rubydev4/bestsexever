class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :game, index: true, foreign_key: true
      t.text :question
      t.string :qtype

      t.timestamps null: false
    end
  end
end
