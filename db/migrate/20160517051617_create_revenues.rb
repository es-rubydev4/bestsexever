class CreateRevenues < ActiveRecord::Migration
  def change
    create_table :revenues do |t|
      t.float :amount, default: 0

      t.timestamps null: false
    end
  end
end
