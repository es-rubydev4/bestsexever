class CreatePlayedQuestions < ActiveRecord::Migration
  def change
    create_table :played_questions do |t|
      t.references :played_game, index: true, foreign_key: true
      t.references :question, index: true, foreign_key: true
      t.references :message, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
