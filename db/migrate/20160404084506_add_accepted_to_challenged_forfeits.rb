class AddAcceptedToChallengedForfeits < ActiveRecord::Migration
  def change
    add_column :challenged_forfeits, :accepted, :boolean
  end
end
