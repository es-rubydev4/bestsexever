class CreatePlays < ActiveRecord::Migration
  def change
    create_table :plays do |t|
      t.references :user, index: true, foreign_key: true
      t.references :conversation, index: true, foreign_key: true
      t.string :rps
      t.boolean :read , default: false

      t.timestamps null: false
    end
  end
end
