# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160623052152) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "answers", force: :cascade do |t|
    t.integer  "question_id"
    t.text     "option"
    t.boolean  "correct",     default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id"

  create_table "bought_games", force: :cascade do |t|
    t.integer  "game_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bought_games", ["game_id"], name: "index_bought_games_on_game_id"
  add_index "bought_games", ["user_id"], name: "index_bought_games_on_user_id"

  create_table "challenged_forfeits", force: :cascade do |t|
    t.integer  "forfeit_id"
    t.integer  "user_id"
    t.integer  "recipient_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "accepted"
    t.integer  "conversation_id"
    t.boolean  "completed",       default: false
  end

  add_index "challenged_forfeits", ["conversation_id"], name: "index_challenged_forfeits_on_conversation_id"
  add_index "challenged_forfeits", ["forfeit_id"], name: "index_challenged_forfeits_on_forfeit_id"
  add_index "challenged_forfeits", ["recipient_id"], name: "index_challenged_forfeits_on_recipient_id"
  add_index "challenged_forfeits", ["user_id"], name: "index_challenged_forfeits_on_user_id"

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "sender_accepted",    default: false
    t.boolean  "recipient_accepted", default: false
    t.integer  "count",              default: 0
    t.boolean  "sender_read",        default: false
    t.boolean  "recipient_read",     default: false
  end

  add_index "conversations", ["recipient_id"], name: "index_conversations_on_recipient_id"
  add_index "conversations", ["sender_id"], name: "index_conversations_on_sender_id"

  create_table "forfeits", force: :cascade do |t|
    t.text     "title"
    t.integer  "cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "games", force: :cascade do |t|
    t.text     "title"
    t.text     "cost"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "free",       default: false
    t.integer  "sn"
  end

  create_table "invitations", force: :cascade do |t|
    t.boolean  "accepted",        default: false
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.integer  "conversation_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "invitations", ["conversation_id"], name: "index_invitations_on_conversation_id"
  add_index "invitations", ["recipient_id"], name: "index_invitations_on_recipient_id"
  add_index "invitations", ["sender_id"], name: "index_invitations_on_sender_id"

  create_table "messages", force: :cascade do |t|
    t.integer  "conversation_id"
    t.boolean  "user2_read",      default: false
    t.boolean  "user1_read",      default: false
    t.integer  "winner_id"
    t.integer  "loser_id"
    t.integer  "user1_id"
    t.integer  "user2_id"
    t.boolean  "rps"
    t.boolean  "answered"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "question_id"
    t.integer  "answer_id"
  end

  add_index "messages", ["answer_id"], name: "index_messages_on_answer_id"
  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id"
  add_index "messages", ["loser_id"], name: "index_messages_on_loser_id"
  add_index "messages", ["question_id"], name: "index_messages_on_question_id"
  add_index "messages", ["user1_id"], name: "index_messages_on_user1_id"
  add_index "messages", ["user2_id"], name: "index_messages_on_user2_id"
  add_index "messages", ["winner_id"], name: "index_messages_on_winner_id"

  create_table "played_answers", force: :cascade do |t|
    t.integer  "played_question_id"
    t.integer  "answer_id"
    t.integer  "message_id"
    t.integer  "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "played_answers", ["answer_id"], name: "index_played_answers_on_answer_id"
  add_index "played_answers", ["message_id"], name: "index_played_answers_on_message_id"
  add_index "played_answers", ["played_question_id"], name: "index_played_answers_on_played_question_id"
  add_index "played_answers", ["user_id"], name: "index_played_answers_on_user_id"

  create_table "played_games", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "game_id"
    t.integer  "conversation_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "played_games", ["conversation_id"], name: "index_played_games_on_conversation_id"
  add_index "played_games", ["game_id"], name: "index_played_games_on_game_id"
  add_index "played_games", ["user_id"], name: "index_played_games_on_user_id"

  create_table "played_questions", force: :cascade do |t|
    t.integer  "played_game_id"
    t.integer  "question_id"
    t.integer  "message_id"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "played_questions", ["message_id"], name: "index_played_questions_on_message_id"
  add_index "played_questions", ["played_game_id"], name: "index_played_questions_on_played_game_id"
  add_index "played_questions", ["question_id"], name: "index_played_questions_on_question_id"
  add_index "played_questions", ["user_id"], name: "index_played_questions_on_user_id"

  create_table "plays", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "conversation_id"
    t.string   "rps"
    t.boolean  "read",            default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "plays", ["conversation_id"], name: "index_plays_on_conversation_id"
  add_index "plays", ["user_id"], name: "index_plays_on_user_id"

  create_table "questions", force: :cascade do |t|
    t.integer  "game_id"
    t.text     "question"
    t.string   "qtype"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "male"
    t.boolean  "female"
    t.text     "image"
    t.string   "headline"
    t.integer  "sn"
  end

  add_index "questions", ["game_id"], name: "index_questions_on_game_id"

  create_table "revenues", force: :cascade do |t|
    t.float    "amount",     default: 0.0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "rps_questions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "conversation_id"
    t.integer  "question_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "rps_questions", ["conversation_id"], name: "index_rps_questions_on_conversation_id"
  add_index "rps_questions", ["question_id"], name: "index_rps_questions_on_question_id"
  add_index "rps_questions", ["user_id"], name: "index_rps_questions_on_user_id"

  create_table "scores", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "conversation_id"
    t.integer  "point",           default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "scores", ["conversation_id"], name: "index_scores_on_conversation_id"
  add_index "scores", ["user_id"], name: "index_scores_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "api_key"
    t.string   "name"
    t.string   "gender"
    t.string   "avatar"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
