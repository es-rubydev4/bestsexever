ActiveAdmin.register Conversation do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
  actions :all, :except => [:new, :edit]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    column :id
    column :sender
    column :sender_accepted
    column :recipient
    column :recipient_accepted
    actions :defaults => true

  end


  show do |c|
    columns do
      # column do
      #   attributes_table do
      #     row :id
      #     row :sender
      #     row :recipient
      #     row :created_at
      #     row :updated_at
      #     row :sender_accepted
      #     row :sender_read
      #     row :recipient_accepted
      #     row :recipient_read
      #     row "No. of Games Played" do |c|
      #       c.count
      #     end
      #   end
      # end

      panel "Detail" do
        table_for "Forfeits Sent 2" do
            ul do
              li span "Id: #{c.id}"
              li link_to("Sender: #{c.sender.name}", admin_user_path(c.sender.id))
              li link_to("Recipient: #{c.recipient.name}", admin_user_path(c.recipient.id))
              li span "Created At: #{c.created_at.to_formatted_s(:long)}"
              li span "Updated At: #{c.updated_at.to_formatted_s(:long)}"
              li span "Sender Accepted: #{c.sender_accepted}"
              li span "Sender Read: #{c.sender_read}"
              li span "Recipient Accepted: #{c.recipient_accepted}"
              li span "Recipient Read: #{c.recipient_read}"
              li span "No. of Games Played: #{c.count}"
            end
        end
      end




        column do
          panel "#{c.sender.name}\'s details" do

            table_for "Conversation Detail" do

              column "Score" do
                span "#{c.sender.scores.where(conversation_id: c.id).present? ? c.sender.scores.where(conversation_id: c.id).last.point : 0}"
              end

              column "Played Game" do
                c.sender.played_games.where(conversation_id: c.id).each do |pg|
                  span link_to("Level #{pg.game.title}", admin_game_path(pg.game.id))
                  ol do
                    span "Question/Activity played " if pg.played_questions.present?
                    pg.played_questions.reverse[0...20].each do |pq|
                      # table_for "Conversation Detail" do
                      #   column "Question/Activity" do
                      li span link_to("Q: #{pq.question.question}", admin_question_path(pq.question.id))
                      span "Answered: " if pq.played_answer
                      pq.played_answer.each_with_index do |a |
                        # span " #{a.answer.option}. "
                        span link_to("#{a.answer.option + (a.answer.correct ? " (√)" : " " ) }", admin_answer_path(a.answer.id))

                      end
                      # end
                      # end
                      # span "Question #{pg.game.id} "
                    end
                  end

                end
              end
              column "Forfeits Sent" do
                c.sender.challenged_forfeits(c).each do |cf|
                  table_for "Forfeits Sent 2" do
                    column do
                      span link_to("#{cf.forfeit.title}", admin_forfeit_path(cf.forfeit.id))
                      ul do
                        li span cf.accepted ? "Accepted" : (cf.accepted.nil? ? "Pending" : "Declined ")
                        li span  "Completed" if cf.completed?
                      end
                    end
                  end

                end
              end

              column "Forfeits Received" do
                c.sender.forfeits_received(c).each do |rf|
                  table_for "Forfeits Received 2" do
                    column do
                      span link_to("#{rf.forfeit.title}", admin_forfeit_path(rf.forfeit.id))
                      ul do
                        li span rf.accepted ? "Accepted" : (rf.accepted.nil? ? "Pending" : "Declined ")
                        li span  "Completed"  if rf.completed?
                      end
                    end
                  end

                end
              end


            end
          end
          panel "#{c.recipient.name}\'s details" do

            table_for "Conversation Detail" do

              column "Score" do
                span "#{c.recipient.scores.where(conversation_id: c.id).present? ? c.recipient.scores.where(conversation_id: c.id).last.point : 0}"
              end

              column "Played Game" do
                c.recipient.played_games.where(conversation_id: c.id).each do |pg|
                  span link_to("Level #{pg.game.title}", admin_game_path(pg.game.id))
                  ol do
                    span "Question/Activity played " if pg.played_questions.present?
                    pg.played_questions.reverse[0...20].each do |pq|
                      # table_for "Conversation Detail" do
                      #   column "Question/Activity" do
                      li span link_to("Q: #{pq.question.question}", admin_question_path(pq.question.id))
                      span "Answered: " if pq.played_answer
                      pq.played_answer.each_with_index do |a |
                        # span " #{a.answer.option}. "
                        span link_to("#{a.answer.option + (a.answer.correct ? " (√)" : " " ) }", admin_answer_path(a.answer.id))

                      end
                      # end
                      # end
                      # span "Question #{pg.game.id} "
                    end
                  end

                end
              end
              column "Forfeits Sent" do
                c.recipient.challenged_forfeits(c).each do |cf|
                  table_for "Forfeits Sent 2" do
                    column do
                      span link_to("#{cf.forfeit.title}", admin_forfeit_path(cf.forfeit.id))
                      ul do
                        li span cf.accepted ? "Accepted" : (cf.accepted.nil? ? "Pending" : "Declined ")
                        li span  "Completed" if cf.completed?
                      end
                    end
                  end

                end
              end

              column "Forfeits Received" do
                c.recipient.forfeits_received(c).each do |rf|
                  table_for "Forfeits Received 2" do
                    column do
                      span link_to("#{rf.forfeit.title}", admin_forfeit_path(rf.forfeit.id))
                      ul do
                        li span rf.accepted ? "Accepted" : (rf.accepted.nil? ? "Pending" : "Declined ")
                        li span "Completed" if rf.completed?
                      end
                    end
                  end

                end
              end


            end
          end
        end

      #   column do
      #   panel "Associated Conversations" do
      #     u.associated_conversation.each do |c|
      #       span class: "blank_slate" do
      #         link_to("Conversation #{c.id}", admin_conversation_path(c.id))
      #       end
      #     end
      #   end
      # end
    end
  end


end
