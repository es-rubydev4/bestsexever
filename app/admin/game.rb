ActiveAdmin.register Game do
  active_admin_importable do |model, hash|
    # begin
      game = Game.where(title: hash[:game_title]).first_or_create
      question = hash[:question].encode('UTF-8', :invalid => :replace, :undef => :replace).gsub "�" , "'"
      question = Question.create(sn: hash[:sn], game_id: game.id, question: question, qtype: hash[:type], image: hash[:image])
      # question = Question.create(game_id: game.id, question: hash[:question], qtype: hash[:type], image: hash[:image])
      if  hash[:gender] == "both"
        question.male = true
        question.female = true
      else
        hash[:gender] == "female" ? question.female = true : question.male = true
      end
      question.headline = hash[:headline] if hash[:headline].present?
      question.save
      if !hash[:answer_1].nil?
        answer = Answer.create(question_id: question.id, option: hash[:answer_1])
        if hash[:answer_1].include?("#answer#")
          answer.option.gsub! '#answer#', ''
          answer.correct = true
        end
        answer.save
      end
      if !hash[:answer_2].nil?
        answer = Answer.create(question_id: question.id, option: hash[:answer_2])
        if hash[:answer_2].include?("#answer#")
          answer.option.gsub! '#answer#', ''
          answer.correct = true
        end
        answer.save
      end
      if !hash[:answer_3].nil?
        answer = Answer.create(question_id: question.id, option: hash[:answer_3])
        if hash[:answer_3].include?("#answer#")
          answer.option.gsub! '#answer#', ''

          answer.correct = true
        end
        answer.save
      end
      if !hash[:answer_4].nil?
        answer = Answer.create(question_id: question.id, option: hash[:answer_4])
        if hash[:answer_4].include?("#answer#")
          answer.option.gsub! '#answer#', ''

          answer.correct = true
        end
        answer.save
      end
    # rescue Exception => e
    #   Rails.logger.error e.message
    #   Rails.logger.error e.backtrace.join("\n")
    # end
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :title, :free ,question_attributes: [:id, :question, :sn, :qtype, :male, :female, :image, :_destroy,
                                                   answers_attributes: [ :id, :option, :_destroy, :correct ]]

#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    column :id
    column :title
    column :free
    actions :defaults => true
  end


  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Details' do
      f.input :title
      f.input :free
    end
    f.inputs do
      f.has_many :question, heading: false, allow_destroy: true do |q|
        q.input :question
        q.input :sn
        q.input :male
        q.input :female
        q.input :qtype, :as => :select, :collection => ["general", "multiple", "confession", "undress", "foreplay", "sex", "finish"], :prompt => true
        q.has_many :answers, heading: false, allow_destroy: true do |a|
          a.input :option
          a.input :correct
        end
      end
    end
    f.actions
  end

  show do |g|

    columns do
      column do
        attributes_table do
          row :id
          row :title
          row :free
          row :created_at
          row :updated_at
          row "Associated Questions" do |u|
            u.question.count
          end
        end
      end

      column do
        # panel "Associated Questions" do
        #   ol do
        #     g.question.each do |a|
        #       span class: "blank_slate" do
        #        li span link_to("#{ a.qtype + " - " + (a.male ? "male" : "female" ) + " - " + a.question }" , admin_question_path(a.id))
        #       end
        #     end
        #   end
        # end

        # panel "Associated Questions" do
        #
        #   table_for "Associated Questions" do
        #     g.question.each do |a|
        #       column "Type" do
        #         span a.qtype
        #       end
        #
        #       column "Gender" do
        #         span "#{a.male ? "Male" : "Female" }"
        #       end
        #
        #       column "Gender" do
        #         li span link_to(a.question , admin_question_path(a.id))
        #       end
        #     end
        #
        #   end
        #
        # end
        panel "Associated Questions" do
          table_for g.question.order('sn asc') do
            column "SN", :sn
            column "Type", :qtype
            column "Gender" do |g|
              g.male ? g.female ? "Both" : "Male" : "Female"
            end
            column "Title" do |t|
              link_to(t.question , admin_question_path(t.id))
            end
          end
        end
      end

    end

    # attributes_table do
    #   g.question.each do |a|
    #     row "Type" do
    #       span a.qtype
    #     end
    #     row "Gender" do
    #       span "#{a.male ? "Male" : "Female" }"
    #     end
    #     row "Question" do
    #       span link_to(a.question , admin_question_path(a.id))
    #     end
    #   end
    # end

  end


end
