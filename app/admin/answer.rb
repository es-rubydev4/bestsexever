ActiveAdmin.register Answer do
  active_admin_importable


# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :option, :correct
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    selectable_column
    column :id
    column :option
    column :correct
    column "Question Type" do |a|
      a.question.qtype
    end
    actions :defaults => true
  end
  show do
    columns do
      column do
        attributes_table do
          row :id
          row "question" do |q|
            q.question.question
            link_to(q.question.question , admin_question_path(q.question.id))
          end
          row :option
          row :correct
          row :created_at
          row :updated_at
        end
      end


    end
  end


end
