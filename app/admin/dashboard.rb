ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      # column do
      #   panel "Recent Posts" do
      #     ul do
      #       Post.recent(5).map do |post|
      #         li link_to(post.title, admin_post_path(post))
      #       end
      #     end
      #   end
      # end

      if Rails.env.production? #cause group_by_date works only on Postgresql
        div class: 'custom-class' do
          h3 'Activity Stats'
          @metric = Message.group_by_day(:created_at, range: 4.weeks.ago.midnight..Time.now).count #whatever data you pass to chart
          render partial: 'metrics/activity', locals: {metric: @metric}
        end
      end



      column do
        #pie chart
        div class: 'custom-class' do
          h3 'User Stats'
          @metric = User.group(:gender).count #whatever data you pass to chart
          render partial: 'metrics/user', locals: {metric: @metric}
        end
      end


      #bar chart
      # div class: 'custom-class' do
      #   h3 'User Stats'
      #   @metric = User.group(:gender).count #whatever data you pass to chart
      #   render partial: 'metrics/user2', locals: {metric: @metric}
      # end

      column do
        # panel "Total Revenue" do
        #   total = 0
        #   BoughtGame.all.each do |bg|
        #     total = total + bg.game.cost.to_i
        #   end
        #   "$ "+ total.to_s
        # end

        panel "Total Revenue" do
          Revenue.count > 0 ? Revenue.first.amount.to_s + " $" : 0.to_s + " $"
        end


        if PlayedGame.count > 1
            panel "Last Game Played" do
              if Message.last.question
                span link_to(Message.last.question.game.title, admin_game_path(Message.last.question.game.id))
                span " between "
                span link_to(Message.last.conversation.sender.name, admin_user_path(Message.last.conversation.sender.id))
                span " and "
                span link_to(Message.last.conversation.recipient.name, admin_user_path(Message.last.conversation.recipient.id))
                span " #{time_ago_in_words(Message.last.created_at)} ago."
              else
                span link_to(PlayedGame.last.game.title, admin_game_path(PlayedGame.last.game.id))
                span " between "
                span link_to(PlayedGame.last.conversation.sender.name, admin_user_path(PlayedGame.last.conversation.sender.id))
                span " and "
                span link_to(PlayedGame.last.conversation.recipient.name, admin_user_path(PlayedGame.last.conversation.recipient.id))
                span " #{time_ago_in_words(PlayedGame.last.created_at)} ago."
              end
            end
        end

        if Message.count > 1
            panel "Last Activity" do
              # span "On "
              # span link_to(Message.last.conversation.id, admin_conversation_path(Message.last.conversation.id))
              span "Between "
              span link_to(Message.last.conversation.sender.name, admin_user_path(Message.last.conversation.sender.id))
              span " and "
              span link_to(Message.last.conversation.recipient.name, admin_user_path(Message.last.conversation.recipient.id))
              span " #{time_ago_in_words(Message.last.created_at)} ago."
            end
        end
      end

      column do
        panel "5 Highest Scorers" do
          ol do
            Score.all.sort_by {|k| k.point }.reverse[0..4].each do |s|
              table do
                li span link_to(s.user.name, admin_user_path(s.user.id))
                span link_to(s.point, admin_conversation_path(s.conversation.id))
              end
            end
          end

        end
      end









      # if Invitation.last.present?
      #   column do
      #     panel "Last Invitation" do
      #       span "Between "
      #       span link_to(Invitation.last.conversation.sender.name, admin_user_path(Message.last.conversation.sender.id))
      #       span " and "
      #       span link_to(Message.last.conversation.recipient.name, admin_user_path(Message.last.conversation.recipient.id))
      #       span " #{time_ago_in_words(Message.last.created_at)} ago."
      #     end
      #   end
      # end

    end
  end # content
end
