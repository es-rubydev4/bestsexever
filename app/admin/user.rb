ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :email, :encrypted_password, :reset_password_token, :reset_password_sent_at, :reset_password_sent_at, :remember_created_at, :current_sign_in_at, :current_sign_in_at, :last_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip, :api_key, :name, :gender, :avatar

  actions :all, :except => [:new]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


# div class: "blank_slate_container", id: "dashboard_default_message" do
#   span class: "blank_slate" do
#     span I18n.t("active_admin.dashboard_welcome.welcome")
#     small I18n.t("active_admin.dashboard_welcome.call_to_action")
#   end
# end
  index do
    selectable_column
    column :id
    column :email
    column :name
    column :gender
    column "Conversations" do |u|
      u.all_conversation.count
    end
    actions :defaults => true
  end

  show do |u|
    columns do
      column do
        attributes_table do
          row "Profile Picture", :avatar do
            if u.avatar.present?
              image_tag(u.avatar.url(:thumb))
            else
              "Not Available"
            end
          end
          row :id
          row :name
          row :email
          row :gender
          row :created_at
          row :last_sign_in_at
        end
      end

      column do
        panel "Games Bought" do
          u.bought_games.each do |bg|
              span class: "blank_slate" do
                span link_to(bg.game.title , admin_game_path(bg.game.id))
              end
          end
        end
      end

      u.all_conversation.each_with_index do | c, index|
        column do
          panel "Associated Conversation #{index+1}" do

            table_for "Conversation Detail" do
              column "ID" do
                link_to(c.id , admin_conversation_path(c.id))
              end

              column "Score" do
                span "#{u.scores.where(conversation_id: c.id).present? ? u.scores.where(conversation_id: c.id).last.point : 0}"
              end

              # column "Played Game" do
              #   user.played_games.where(conversation_id: c.id).each do |pg|
              #     span link_to("Level #{pg.game.title}", admin_game_path(pg.game.id))
              #     ul do
              #       span "Question/Activity played " if pg.played_questions.present?
              #       pg.played_questions.each do |pq|
              #         # table_for "Conversation Detail" do
              #         #   column "Question/Activity" do
              #         li span link_to("Q: #{pq.question.question}", admin_question_path(pq.question.id))
              #         span "Answered: " if pq.played_answers
              #         pq.played_answers.each_with_index do |a, index |
              #           # span " #{a.answer.option}. "
              #           span link_to("#{a.answer.option + (a.answer.correct ? " (√)" : " " ) }", admin_answer_path(a.answer.id))
              #
              #         end
              #         # end
              #         # end
              #         # span "Question #{pg.game.id} "
              #       end
              #     end
              #
              #   end
              # end

              # column "Forfeits Sent" do
              #   user.challenged_forfeits(c).each do |cf|
              #     table_for "Forfeits Sent 2" do
              #       column do
              #         span link_to("#{cf.forfeit.title}", admin_forfeit_path(cf.forfeit.id))
              #         span cf.accepted ? "Accepted" : (cf.accepted.nil? ? "Pending" : "Declined ")
              #       end
              #     end
              #
              #   end
              # end
              #
              # column "Forfeits Received" do
              #   user.forfeits_received(c).each do |rf|
              #     table_for "Forfeits Received 2" do
              #       column do
              #         span link_to("#{rf.forfeit.title}", admin_forfeit_path(rf.forfeit.id))
              #         span rf.accepted ? "Accepted" : (rf.accepted.nil? ? "Pending" : "Declined ")
              #       end
              #     end
              #
              #   end
              # end

            end

            # span class: "blank_slate" do
            #   link_to("Conversation #{c.id}", admin_conversation_path(c.id))
            # end
          end
        end
      end

      #   column do
      #   panel "Associated Conversations" do
      #     u.associated_conversation.each do |c|
      #       span class: "blank_slate" do
      #         link_to("Conversation #{c.id}", admin_conversation_path(c.id))
      #       end
      #     end
      #   end
      # end
    end
  end



end
