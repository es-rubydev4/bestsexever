ActiveAdmin.register Question do
  active_admin_importable

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :game_id,:headline, :question, :qtype, :image, :male, :female, :sn,
                answers_attributes: [ :id, :option, :_destroy, :correct ]
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    column :id
    column "Game" do |u|
      u.game.title
    end
    column :headline
    column :question
    column :qtype
    column :male
    column :female
    actions :defaults => true
  end

  form do |f|
    f.inputs 'Details' do
      f.input :game
      f.input :sn
      f.input :headline
      f.input :question
      f.input :image
      f.input :male
      f.input :female
      f.input :qtype, :as => :select, :collection => ["general", "multiple", "confession", "undress", "foreplay", "sex", "finish"], :prompt => true
    end
    f.inputs do
      f.has_many :answers, heading: false, allow_destroy: true do |a|
      a.input :option
      a.input :correct
      end
    end
    f.actions
  end


  show do |g|
    columns do
      column do
        attributes_table do
          row :id
          row :sn
          row :headline
          row :question
          row :image
          row "Preview", :image do
            if g.image.present?
              u = g.image
              u.slice! "dl=0"
              u = u + "raw=1"
              image_tag(u)
            else
              "Not Available"
            end
          end
          row :game_id
          row :male if g.male == true
          row :female if g.female == true
          row :created_at
          row :updated_at
          row :qtype
        end
      end


      # if g.image?
      #   column do
      #     panel "Image" do
      #       u = g.image
      #       u.slice! "dl=0"
      #       u = u + "raw=1"
      #       image_tag(u)
      #     end
      #   end
      # end

      if g.qtype == "general" || g.qtype == "multiple"
        column do
          panel "Option" do
            ul do
              g.answers.each do |a|
                span class: "blank_slate" do
                  li link_to("#{a.option + (a.correct ? " √" : "" ) }", admin_answer_path(a.id))
                end
              end
            end

          end
        end
      end

    end
  end

end