class PlayedAnswer < ActiveRecord::Base
  belongs_to :played_question
  belongs_to :answer

end
