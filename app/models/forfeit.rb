class Forfeit < ActiveRecord::Base
  validates :title, presence: true
  validates :cost, presence: true
end
