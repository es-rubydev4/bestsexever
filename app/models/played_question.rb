class PlayedQuestion < ActiveRecord::Base
  belongs_to :played_game
  belongs_to :question
  has_many :played_answer, dependent: :destroy

end
