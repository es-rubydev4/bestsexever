class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true,
            length: { minimum: 2 }

  validates :gender, :inclusion => { in: %w(male female),
                                     message: "%{value} is not a valid gender" } , presence: true

  has_many :conversations, :foreign_key => :sender_id , dependent: :destroy
  has_many :invitations, :foreign_key => :sender_id
  has_many :scores, dependent: :destroy
  has_many :rps_questions, dependent: :destroy
  has_many :bought_games, dependent: :destroy
  accepts_nested_attributes_for :conversations, :allow_destroy => true
  accepts_nested_attributes_for :scores, :allow_destroy => true
  has_many :played_games, dependent: :destroy
  has_many :challenged_forfeits, dependent: :destroy
  has_many :plays
  has_many :bought_games

  before_destroy :delete_association



  # access_token for api
  def generate_access_token
    # self.api_key = SecureRandom.base64.tr('+/=', 'Qrt')
    self.api_key = Digest::SHA1.hexdigest("--#{self.email}--#{self.id}--#{Time.now}")
    self.save!
  end

  # device id for api
  def assign_device(user)
    if user[:device_id].present?
      device = Device.where(device_id: user[:device_id]).last
      if device.present?
        self.devices << device
      else
        self.devices.create(device_id: user[:device_id] , platform: user[:device_type])
      end
    end
  end

  #remove device after logout
  def remove_device(device_id)
    self.devices.each do |d|
      self.devices.delete(d) if d.device_id == device_id
    end
  end

  def all_conversation
    (Conversation.where(sender_id: self.id) + Conversation.where(recipient_id: self.id )  ).uniq
  end

  # forfeit received/challeged to a user
  def forfeits_received(conversation)
    ChallengedForfeit.where(recipient_id: self.id, conversation: conversation)
  end

  # forfeit sent/challenged by a user
  def challenged_forfeits(conversation)
    ChallengedForfeit.where(user_id: self.id, conversation: conversation)
  end

  def invite_sent
    Invitation.where(sender_id: self.id)
  end

  def invites
    Invitation.where(recipient_id: self.id).last
  end

  def clear_invites
    Invitation.where(recipient: self.id).each do |i|
      i.destroy
    end
  end


  # user's conversation score
  def conversation_score(conversation_id)
    score = self.scores.where(conversation_id: conversation_id).last
    score.present? ? score.point : 0
  end

  # user's conversation score updating
  def update_score(conversation, pts)
    score = self.scores.where(conversation_id: conversation.id).first_or_create
    score.point = score.point + pts
    score.save!

  end

  # forfeit brought by a user
  def bought_forfeit(conversation_id, forfeit_id)
    score = self.scores.first_or_create(conversation_id: conversation_id)
    forfeit_cost = Forfeit.where(id: forfeit_id).first.cost
    score.point = score.point - forfeit_cost
    score.save!
  end

  def self.set_user(id)
    User.where(id: id).last
  end

  # check if user is male
  def male?
    self.gender == "male" ? true : false
  end

  # check if user is female
  def female?
    self.gender == "female" ? true : false
  end

  # generate rps question with a question list/type/pattern/sequence
  def generate_rps_question(game_id, conversation , types)
    types ||=  %w[multiple multiple confession multiple undress multiple confession undress multiple multiple confession undress multiple confession multiple foreplay undress confession foreplay confession undress confession undress foreplay foreplay confession foreplay sex foreplay sex sex finish]
    types.inject([]) do |memo, type|
      unless type == "undress"
        while memo.include?(
            q = Question.where(game_id: game_id, qtype: type).sample)  do end # skip unless unique
      else
        q = Question.where(game_id: game_id, qtype: type).sample
      end
      rqs = RpsQuestion.create(user: self, question: q, conversation: conversation)
      if rqs.save
        memo << q
      end
    end
  end

  # generate rps question for male
  def generate_rps_question_male(game_id, conversation)
    Question.where(game_id: game_id).where(male: true).where.not(qtype: "general").order('sn asc').each do |q|
      RpsQuestion.create(user: self, question: q, conversation: conversation)
    end
  end

  # generate rps question for female
  def generate_rps_question_female(game_id, conversation)
    Question.where(game_id: game_id).where(female: true).where.not(qtype: "general").order('sn asc').each do |q|
      RpsQuestion.create(user: self, question: q, conversation: conversation)
    end
  end


  # generate rps question for male with a question list/type/pattern/sequence
  def male_generate_rps_question(game_id, conversation)
    types ||=  %w[multiple multiple confession multiple undress multiple confession undress multiple multiple confession undress multiple confession multiple foreplay undress confession foreplay confession undress confession undress foreplay foreplay confession foreplay sex foreplay sex sex finish]
    types.inject([]) do |memo, type|
      unless type == "undress"
        while memo.include?(
            q = Question.where(game_id: game_id, qtype: type, male: true).sample)  do end # skip unless unique
      else
        q = Question.where(game_id: game_id, qtype: type, male: true).sample
      end
      rqs = RpsQuestion.create(user: self, question: q, conversation: conversation)
      if rqs.save
        memo << q
      end
    end
  end

  # generate rps question for female with a question list/type/pattern/sequence
  def female_generate_rps_question(game_id, conversation)
    types ||=  %w[multiple multiple confession multiple undress multiple confession undress multiple multiple confession undress multiple confession multiple foreplay undress confession foreplay confession undress confession undress foreplay foreplay confession foreplay sex foreplay sex sex finish]
    types.inject([]) do |memo, type|
      unless type == "undress"
        while memo.include?(
            q = Question.where(game_id: game_id, qtype: type, female: true).sample)  do end # skip unless unique
      else
        q = Question.where(game_id: game_id, qtype: type, female: true).sample
      end
      rqs = RpsQuestion.create(user: self, question: q, conversation: conversation)
      if rqs.save
        memo << q
      end
    end
  end

  def get_question(message)
    @messages = message
    question = set_question(@messages)
    if @messages.question.answers.present? && @messages.question.qtype == "multiple" # also include answer json if its multiple type question
      @messages.question ? {question: question, answers: @messages.question.answers.as_json(:only => [:id, :option, :correct])} : { }
    else
      @messages.question ? {question: question} : { }
    end
  end

  def set_question(message)
    @messages = message
    if @messages.question.question.include?("#female#") || @messages.question.question.include?("#male#")
      if @messages.question.question.include?("#female#") || @messages.question.question.include?("#female#,") || @messages.question.question.include?("#female#.") # replace female with user's name
        question_title = @messages.question.question.gsub! '#female#', @messages.conversation.sender.gender == "male" ? @messages.conversation.recipient.name : @messages.conversation.sender.name

      end
      if @messages.question.question.include?("#male#") || @messages.question.question.include?("#male#,") || @messages.question.question.include?("#male#.") # replace male with user's name
        question_title = @messages.question.question.gsub! '#male#', @messages.conversation.sender.gender == "female" ? @messages.conversation.recipient.name : @messages.conversation.sender.name
      end

    else
      question_title = @messages.question.question
    end
    {id: @messages.question.id, question: question_title.strip, qtype: @messages.question.qtype }
  end


  def for_answered(message)
    @messages = message
    results = {action: "#{@messages.loser == @api_user ? "you" : @messages.loser.name } answered"  }
    #if api_user is user1 to message then set answer_it to true if user1 hasn't read the message , else if api_user is user2 to message then set answer_it to true if user2 hasn't read the message
    @api_user == @messages.user1 ? reading = {self_read: @messages.user1_read , partner_read: @messages.user2_read, answer_it: !@messages.user1_read  } : reading = {self_read: @messages.user2_read , partner_read: @messages.user1_read, answer_it: !@messages.user2_read }
    @messages.winner == @api_user ? ack = true : ack = false # true if api_user is winner to the message
    {message_id: @messages.id}.merge(reading).merge(results).merge(ack: ack).merge(question_type: @messages.question.qtype).merge(question: set_question(@messages), answers: @messages.answer.as_json(:only => [:id, :option]))
  end

  def for_unanswered(message)
    @messages = message
    #if api_user is user1 to message then set answer_it to true if user1 hasn't read the message , else if api_user is user2 to message then set answer_it to true if user2 hasn't read the message
    @api_user == @messages.user1 ? reading = {self_read: @messages.user1_read , partner_read: @messages.user2_read, answer_it: !@messages.user1_read  } : reading = {self_read: @messages.user2_read , partner_read: @messages.user1_read, answer_it: !@messages.user2_read }
    results = {general_question: true, action: "both to answer"  }
    {message_id: @messages.id}.merge(reading).merge(results).merge(question_type: @messages.question.qtype).merge(question: set_question(@messages), answers: @messages.question.answers.as_json(:only => [:id, :option, :correct]))
  end

  def for_winner(message)
    @messages = message
    questions = get_question(@messages)
    if questions[:question][:qtype] == "finish" # if the question is of type finish i.e. last question of game
      results = {result: "you won", answer_it: false, action: "#{@messages.loser.name} to answer"  }
      ack = false # acknowledge is set to false , since loser has to ack that
    else
      results = {result: "you won", answer_it: false, action: "#{@messages.loser.name} to answer"  }
      if @messages.question && !@messages.question.qtype == "multiple" # if message's question is multiple_choice
        @messages.winner == @api_user ? ack = true : ack = false # acknowledge is set to true , since winner has to ack this
      else
        ack = @messages.question.qtype != "multiple"  ? true : false # acknowledge is set to true for non multiple_choice , since winner has to ack this
        # @api_user == @messages.user1 ? @messages.update_attributes(user1_read: true) : @messages.update_attributes(user2_read: true)
      end
    end

    # rps = {partner: @messages.loser.plays.last.rps , your: @messages.winner.plays.last.rps}
    @api_user == @messages.user1 ? read = {self_read: @messages.user1_read , partner_read: @messages.user2_read } : read = {self_read: @messages.user2_read , partner_read: @messages.user1_read }
    {message_id: @messages.id}.merge(read).merge(results).merge(ack: ack).merge(question_type: @messages.question.qtype).merge(image: @messages.question.image ? @messages.question.image : false ).merge(headline: @messages.question.headline ? @messages.question.headline : false).merge(questions)
  end

  def for_draw(message)
    @messages = message
    @api_user == @messages.user1 ? @messages.update_attributes(user1_read: true) : @messages.update_attributes(user2_read: true)
    @api_user == @messages.user1 ? read = {self_read: @messages.user1_read , partner_read: @messages.user2_read } : read = {self_read: @messages.user2_read , partner_read: @messages.user1_read }
    rps = {partner: @messages.user1.plays.last.rps , your: @messages.user2.plays.last.rps}
    {message_id: @messages.id}.merge(general_question: false, action: "draw, retry" ).merge(rps: rps).merge(read)
  end

  def for_loser(message)
    @messages = message
    questions = get_question(@messages)
    if questions[:question][:qtype] == "finish" # if the question is of type finish i.e. last question of game
      # acknowledge is set to true , since loser has to ack this
      results = {result: "you lost", answer_it: true, action: "you to answer", ack: true }
    else
      # acknowledge is set to false , since winner has to ack this
      results = {result: "you lost", answer_it: true, action: "you to answer", ack: false }
    end
    # rps = {partner: @messages.winner.plays.last.rps , your: @messages.loser.plays.last.rps}
    # user1_read is set to true if loser is user1 else set user2_read to true
    @messages.user1 == @api_user ? @messages.update_attributes(user1_read: true) : @messages.update_attributes(user2_read: true)
    @api_user == @messages.user1 ? read = {self_read: @messages.user1_read , partner_read: @messages.user2_read } : read = {self_read: @messages.user2_read , partner_read: @messages.user1_read }
    {message_id: @messages.id}.merge(read).merge(results).merge(question_type: @messages.question.qtype).merge(image: @messages.question.image ? @messages.question.image : false ).merge(headline: @messages.question.headline ? @messages.question.headline : false).merge(questions)
  end

  def for_general_question(message)
    @messages = message
    {message_id: @messages.id}.merge(question: @messages.question.as_json(only: [:id, :question, :qtype]), answers: @messages.question.answers.as_json(:only => [:id, :option, :correct]))
  end


  def show_detail(conversation)
    # https://scontent-hkg3-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/12994326_1020424878006390_2274554095490874481_n.jpg?oh=d96b89a9b215ce67f037b896c1592df9&oe=5779E726
    @conversation = conversation
    @api_user = self
    @reciever = @api_user == @conversation.recipient ? @conversation.sender : @conversation.recipient
    # get the last message belonging to the conversation unread by both or either of user
    @messages = @conversation.messages.where("user1_read =? or user2_read = ?" , false , false).last
    @recipient = User.set_user(@conversation.recipient_id)
    success = {:message => "success", error: false}
    # checks if there are any rps question left to be played , rps question are deleted one by one during result api call on game controller's result method.
    not_game_over = RpsQuestion.where(user: @conversation.recipient, conversation: @conversation).present? && RpsQuestion.where(user: @conversation.sender, conversation: @conversation).present?
    # checks if user can reselect game level , unless one question has been played by one of users, game level can't reselected
    if @api_user.rps_questions.present?
      game_question_count = @api_user.rps_questions.last.question.game.question.count/2
      can_select = (@api_user.rps_questions.count < game_question_count  ? true : false) || (@reciever.rps_questions.count < game_question_count  ? true : false)
    else
      can_select = true
    end
    # number of times game level has been played/selected , either completely or partially
    game_played = @conversation.count
    current_level = @api_user.rps_questions.where(conversation: @conversation).present? ? @api_user.rps_questions.where(conversation: @conversation).last.question.game.title : "0"
    status = {current_level: current_level, game_played: game_played, can_select_game: can_select, game_over: !not_game_over,sender_read: @conversation.sender_accepted, recipient_read: @conversation.recipient_accepted, conversation_id:@conversation.id, own_score: @api_user.conversation_score(@conversation.id), own_name: @api_user.name, partner_id: @reciever.id, partner_name:  @reciever.name, partner_score: @reciever.conversation_score(@conversation.id) }

    if @api_user.general_questions(@conversation) # for there are general question left
      ques = []
      if @api_user.general_questions(@conversation).count > 3 # if there are more than 3 general question , may be pending from previous game play
        @api_user.general_questions(@conversation) - @api_user.general_questions(@conversation)[-3..-1].each do |a|
          Message.find(a.id).destroy # deletes general question other than last 3 general question
        end
        @api_user.general_questions(@conversation).each do |gq|
          ques << for_general_question(gq) # returns general question
        end
      else
        @api_user.general_questions(@conversation).each do |gq|
          ques << for_general_question(gq) # returns general question
        end
      end

      results = {question_type: "general", action: "both to answer"  }
      questions = {questions: ques}
      if ques.present?
        success.as_json.merge(results).merge(show_rps: false).merge(questions).merge(status: status) # show_rps: false , means dont let user play rock-paper-scissor game yet cause there are general question yet to be answered by either of user
      else
        success.as_json.merge(show_rps: false).merge(status: status)
      end
    else # for there are no general question left , i.e. for rps questions
      if @messages.present? && !( @messages.user1_read && @messages.user2_read ) # for there is a message and it hasn't been read by both user
        if !@messages.rps # for non question

          # @messages.user1 == @api_user ? read = {respond: @messages.user1_read, complete: false } : read = {respond: @messages.user2_read, complete: true }
          # @messages.user1 == @api_user ? @messages.update_attributes(user2_read: true) : @messages.update_attributes(user1_read: true)

          if @messages.answered # for answers of rps question , e.g. shows what loser answered to a previous multiple choice question
            success.as_json.merge(for_answered(@messages)).merge(status: status)
          else #complete, this is not used in app yet, i guess :P
            success.as_json.merge(for_unanswered(@messages)).merge(status: status)
          end
        else
          if @messages.winner == @api_user # for api_user is winner of the message
            success.as_json.merge(for_winner(@messages)).merge(status: status)
          elsif !@messages.winner && !@messages.loser # for draw, this is not used in app ever, yet !
            success.as_json.merge(for_draw(@messages)).merge(status: status)
          else # for api_user is loser of the message
            success.as_json.merge(for_loser(@messages)).merge(status: status)
          end
        end
      else
        {message: "success", error: false, show_rps: not_game_over == true ? true : false  ,status: status}
      end
    end

  end


  def general_questions(conversation)
    if conversation.messages.where("user1_read =? or user2_read = ?" , false , false).any? { |m| m.question.qtype == "general"} # if there are any unread general question
      messages = conversation.messages.where("user1_read =? or user2_read = ?" , false , false)
      msg = []
      messages.each do |m|
        if m.user1 == self
          unless m.user1_read # if api_user is message's user1
            if m.question.qtype == "general" # if the question is general question use it
              msg << m
            else # if the question is not general , delete it because its from previous game played that needs to be cleared and we only need general question here.
              m.destroy
            end

          end
        else
          unless m.user2_read # if api_user is message's user2
            if m.question.qtype == "general"  # if the question is general question use it
              msg << m
            else # if the question is not general , delete it because its from previous game played that needs to be cleared and we only need general question here.
              m.destroy
            end
          end
        end
      end
      msg # returns general questions array
    else
      false # returns false , meaning there are not general questions to be answered
    end

  end


  def general_question_for(conversation, game) # general question for particular game , returns true or false
    messages = conversation.messages.where("user1_read =? or user2_read = ?" , false , false)
    if messages.any? { |m| m.question.qtype == "general"}
      msg = []
      messages.each do |m|
        if m.user1 == self
          unless m.user1_read
            msg << m
          end
        else
          unless m.user2_read
            msg << m
          end
        end
      end
      msg.any?{|q| q.question.game == game }
    else
      false
    end
  end

  def clear_general_questions(conversation) # clear all general question for a conversation
    self.general_questions(conversation).each do |q|
      q.destroy
    end
  end

  def clear_rps_questions(conversation)# clear all rps question for a conversation
    RpsQuestion.where(user: self, conversation: conversation).each do |q|
      q.destroy
    end
  end


  private

  def delete_association
    self.all_conversation.each do |c|
      c.destroy
    end

  end

end
