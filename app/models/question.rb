class Question < ActiveRecord::Base
  belongs_to :game
  has_many :answers, dependent: :destroy
  accepts_nested_attributes_for :answers, :allow_destroy => true
  has_many :played_question, dependent: :destroy
  has_many :message, dependent: :destroy
  has_many :rps_question, dependent: :destroy





  # validates :qtype, :inclusion => { in: %w(general multiple confession undress foreplay sex finish),
  #                                   message: "%{value} is not a valid type" } , presence: true
  #
  # validates :question , presence: true
  # validates :game_id, presence: true

  def self.generate_general_question(game_id)
    if Question.where(game_id: game_id, qtype: "general").count > 2
      Question.where(game_id: game_id, qtype: "general").sample(3)
    else
      Question.last
    end
  end


  def self.get_question(question_id)
    Question.where(id: question_id).last
  end



end
