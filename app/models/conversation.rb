class Conversation < ActiveRecord::Base
  belongs_to :sender, :foreign_key => :sender_id, class_name: 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: 'User'

  has_many :messages, dependent: :destroy
  has_many :invitations , dependent: :destroy
  has_many :score , dependent: :destroy
  has_many :rps_question, dependent: :destroy
  has_many :challenged_forfeit, dependent: :destroy
  has_many :played_game, dependent: :destroy


  validates_uniqueness_of :sender_id, :scope => :recipient_id

  scope :involving, -> (user) do
    where("conversations.sender_id =? OR conversations.recipient_id =?",user.id,user.id)
  end

  scope :between, -> (sender_id,recipient_id) do
    where("(conversations.sender_id = ? AND conversations.recipient_id =?) OR (conversations.sender_id = ? AND conversations.recipient_id =?)", sender_id,recipient_id, recipient_id, sender_id)
  end

  def self.set_conversation(conversation_id)
    Conversation.where(id: conversation_id).last
  end

  def self.update_count(conversation)
    conversation = Conversation.where(id: conversation.id).first
    conversation.count = conversation.count + 1
    conversation.save
  end


end
