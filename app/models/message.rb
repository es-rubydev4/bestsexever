class Message < ActiveRecord::Base
  belongs_to :winner, :foreign_key => :winner_id, class_name: 'User'
  belongs_to :loser, :foreign_key => :loser_id, class_name: 'User'
  belongs_to :user1, :foreign_key => :user1_id, class_name: 'User'
  belongs_to :user2, :foreign_key => :user2_id, class_name: 'User'
  belongs_to :conversation, :foreign_key => :conversation_id, class_name: 'Conversation'
  # belongs_to :user
  belongs_to :question
  belongs_to :answer
  has_one :played_question, dependent: :destroy


  validates_presence_of :conversation_id

  def self.reset(conversation)
    self.where(conversation_id: conversation.id).each do |m|
      m.update(user1_read: true, user2_read: true)
    end
  end

end