class Play < ActiveRecord::Base
  belongs_to :user
  belongs_to :conversation


  def self.play_rps(conversation, recipient, sender)

    if recipient && sender
      if recipient.rps == 'R' && sender.rps == 'R'
        winner = false
        loser = false
      elsif recipient.rps == 'R' && sender.rps == 'P'
        winner = conversation.sender
        loser = conversation.recipient
      elsif recipient.rps == 'P' && sender.rps == 'R'
        winner = conversation.recipient
        loser = conversation.sender
      elsif recipient.rps == 'R' && sender.rps == 'S'
        winner = conversation.recipient
        loser = conversation.sender
      elsif recipient.rps == 'S' && sender.rps == 'R'
        winner = conversation.sender
        loser = conversation.recipient
      elsif recipient.rps == 'P' && sender.rps == 'P'
        winner = false
        loser = false
      elsif recipient.rps == 'P' && sender.rps == 'S'
        winner = conversation.sender
        loser = conversation.recipient
      elsif recipient.rps == 'S' && sender.rps == 'P'
        winner = conversation.recipient
        loser = conversation.sender
      elsif recipient.rps == 'S' && sender.rps == 'S'
        winner = false
        loser = false
      end
      recipient.update_attributes(read: true)
      sender.update_attributes(read: true)
      if winner && loser
        winner.update_score(conversation , 10)
        loser.update_score(conversation, -5)
        question = RpsQuestion.where(user: loser, conversation: conversation).first
        msg1 = conversation.messages.build(question: question.question , winner: winner , loser: loser, rps: true, user1: winner, user2: loser )
        msg1.save
        msg2 = conversation.messages.build(question: false , winner: winner , loser: loser, rps: true, user1: winner, user2: loser )
        msg2.save
        question.destroy
      else
        msg = conversation.messages.build(question: Question.last , winner: false , loser: false, rps: true, user1: conversation.sender, user2: conversation.recipient )
        msg.save
      end
    end
  end


end
