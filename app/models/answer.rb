class Answer < ActiveRecord::Base
  belongs_to :question
  has_many :played_answer, dependent: :destroy
  has_many :message, dependent: :destroy



  def self.get_answer(answer_id)
    Answer.where(id: answer_id).last
  end
end
