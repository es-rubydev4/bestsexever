class Invitation < ActiveRecord::Base
  belongs_to :sender, :foreign_key => :sender_id, class_name: 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: 'User'
  belongs_to :conversation, :foreign_key => :conversation_id, class_name: 'Conversation'


  def self.clear(sender_id, recipient_id)
    @invitations = Invitation.where(recipient_id: recipient_id, sender_id: sender_id).where(accepted: true)
    @invitations.each do |e|
      e.destroy
    end
  end

end
