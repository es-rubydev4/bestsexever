class ChallengedForfeit < ActiveRecord::Base
  # belongs_to :sender, :foreign_key => :user_id, class_name: 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: 'User'
  belongs_to :user

  belongs_to :forfeit
  belongs_to :conversation
end
