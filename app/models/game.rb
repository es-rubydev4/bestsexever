class Game < ActiveRecord::Base
  has_many :question, dependent: :destroy
  has_many :played_game, dependent: :destroy
  has_many :bought_game, dependent: :destroy
  accepts_nested_attributes_for :question, :allow_destroy => true

  # validates :title, presence: true


  def self.set_game(game_id)
    Game.where(id: game_id).last
  end

end
