class PlayedGame < ActiveRecord::Base
  belongs_to :game
  has_many :played_questions, dependent: :destroy
  belongs_to :conversation, :foreign_key => :conversation_id, class_name: 'Conversation'

end
