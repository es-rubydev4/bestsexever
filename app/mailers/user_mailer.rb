class UserMailer < ActionMailer::Base
  default :from => "hello@emilyssecret.com"

  def welcome(user)
    @user = user
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Emily's Secret app - Welcome")
  end

  def forfeit(user, forfeit, sender)
    @user = user
    @forfeit = forfeit.title
    @sender = sender
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Emily's Secret challenge")
  end

  def accepted_forfeit(user, forfeit, sender)
    @user = user
    @forfeit = forfeit.title
    @sender = sender
    mail(:to => "#{user.name} <#{user.email}>", :subject => "Emily's Secret challenge")
  end

end