module API
  module V1
    class Users < Grape::API
      version 'v1' # path-based versioning by default
      format :json

      resource :users do


        # desc "Sign up"
        # post "/sign_up"  do
        #   user = User.new({
        #                       name:params[:name],
        #                       gender:params[:gender].downcase,
        #                       email:params[:email],
        #                       password:params[:password],
        #                       password_confirmation:params[:password]
        #                   })
        #
        #   if user.save
        #     user.generate_access_token
        #     user.as_json(:only => [ :email, :name, :id, :api_key]).merge({:message => "success" , error: false })
        #   else
        #     {:errors => user.errors.full_messages.to_sentence, error: true}
        #   end
        # end


        # desc "Sign in"
        # post "/sign_in"  do
        #   user = User.where(email: params[:email]).first
        #   password = params[:password]
        #   if user
        #     if user.valid_password?(password)
        #       user.generate_access_token
        #       user.as_json(:only => [:api_key]).merge({:message => "success", :user_id => user.id, error: false})
        #     else
        #       {:errors => "Password incorrect", :new_user => false, error: true}
        #     end
        #   else
        #     {:errors => "User does not exist", :new_user => true, error: true }
        #   end
        # end

        desc "new Sign Un"
        post "/sign_up"  do
          user = User.where(email: params[:email]).first
          password = params[:password]

          if user
            if user.valid_password?(password)
              user.generate_access_token
              user.update( name: params[:name], gender: params[:gender])
              user.as_json(:only => [:id, :api_key, :name, :email, :gender]).merge({:message => "success", error: false})
            else
              {:errors => "Password incorrect", :new_user => false, error: true}
            end
          else
            user = User.new({
                                name:params[:name],
                                gender:params[:gender].downcase,
                                email:params[:email],
                                password:params[:password],
                                password_confirmation:params[:password]
                            })
            if user.save
              user.generate_access_token
              UserMailer.welcome(user).deliver_now
              user.as_json(:only => [ :email, :name, :id, :api_key, :gender]).merge({:message => "success" , error: false })
            else
              {:errors => user.errors.full_messages.to_sentence, error: true}
            end
          end
        end


        desc "user_detail"
        post "user_detail" do
          authenticate!
          user = User.where(id: params[:id]).last
          if user
            user.as_json(:only => [:email, :id, :name]).merge({:message => "success", :own => @api_user == user })
          else
            {:error => "User does not exist"}
          end
        end


        desc "user update"
        put "name_update" do
          authenticate!
          if @api_user.update( name: params[:name])
            @api_user.as_json(:only => [ :email, :name, :id]).merge({:message => "success"})
          else
            {:error => user.errors.full_messages.to_sentence}
          end
        end


        desc "invite user by email"
        post "invite" do
          authenticate!
          user = User.where(email: params[:email]).last
          if @api_user != user
            if user
              user.as_json(:only => [:email, :name, :gender]).merge({:message => "success" , recipient_id: user.id, error: false })
            else
              {:errors => "User does not exist", error: true}
            end
          else
            {:errors => "Can't invite yourself", error: true}
          end
        end



        desc "send conversation invitation by email"
        post "connect" do
          authenticate!
          user = User.where(id: params[:recipient_id]).last
          if user
            unless @api_user == user
              @conversation = Conversation.between(@api_user.id ,user.id).first_or_create(sender_id: @api_user.id, recipient_id: user.id)
              if @conversation.sender_accepted && @conversation.recipient_accepted
                if @conversation.sender_read && @conversation.recipient_read
                  @conversation.sender_accepted = false
                  @conversation.recipient_accepted = false
                  @conversation.sender_read = false
                  @conversation.recipient_read = false
                  @conversation.sender.clear_rps_questions(@conversation)
                  @conversation.recipient.clear_rps_questions(@conversation)
                  Message.reset(@conversation)
                end
              end
              if @conversation.sender == @api_user
                @conversation.sender_accepted = true
              elsif @conversation.recipient == @api_user
                @conversation.recipient_accepted = true
              end
              if @conversation.save
                @conversation.as_json(:only => [:recipient_id, :sender_id ]).merge({:message => "success", conversation_id: @conversation.id})
              else
                {:error => @conversation.errors.full_messages.to_sentence}
              end
            else
              {:errors => "can't invite self", error: true }
            end
          else
            {:errors => "no such user", error: true }
          end

        end




      end


  end
  end
end



