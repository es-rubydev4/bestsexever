module API
  module V1
    class Forfeits < Grape::API
      version 'v1' # path-based versioning by default
      format :json
      before do
        error!('Unauthorized. Invalid or expired token.', 200) unless current_user
      end


      resource :forfeits do

        desc "list of forfeits"
        get "list" do
          forfeits = Forfeit.all.map{|f| {id: f.id , title: f.title, cost: f.cost}}.group_by{|f| f[:cost]}
          success = {:message => "success", error: false}
          success.as_json.merge(forfeits: forfeits)
        end

        desc "challenge a forfeits"
        post "send" do
          forfeit_cost = Forfeit.where(id: params[:forfeit_id]).first.cost
          conversation = Conversation.where(id: params[:conversation_id]).last
          if @api_user.conversation_score(conversation.id) >= forfeit_cost
            recipient = User.where(id: params[:recipient_id]).last
            forfeit = Forfeit.where(id: params[:forfeit_id]).last
            challenge_forfeit = ChallengedForfeit.create(conversation_id: params[:conversation_id],forfeit_id: forfeit.id, recipient_id: recipient.id, user_id: @api_user.id)
            if challenge_forfeit.save
              UserMailer.forfeit(recipient, forfeit, @api_user).deliver_now if params[:email]
              @api_user.update_score(conversation, -forfeit.cost)
              # @api_user.bought_forfeit(params[:conversation_id], params[:forfeit_id])
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            {:errors => "not enough points", error: true}
          end

        end

        desc "forfeits challenges sent on a conversation"
        post "sent" do
          conversation = Conversation.where(id: params[:conversation_id]).first
          success = {:message => "success", error: false}
          forfeits = @api_user.challenged_forfeits(conversation).map{ |f| {challenged_forfeit_id: f.id, forfeit: f.forfeit.title, cost: f.forfeit.cost, forfeit_id: f.forfeit.id ,recipient_id: f.user_id, recipient: User.where(id: f.recipient_id).first.name, date: f.created_at.strftime("%m/%d/%y"), completed: f.completed , accepted: f.accepted ? true : ( f.accepted.nil? ? "pending" : false) }}
          success.as_json.merge(forfeits: forfeits)
        end

        desc "forfeits challenges received on a conversation"
          post "received" do
            conversation = Conversation.where(id: params[:conversation_id]).first
            success = {:message => "success", error: false}
            forfeits = @api_user.forfeits_received(conversation).map{ |f| {challenged_forfeit_id: f.id, forfeit: f.forfeit.title, cost: f.forfeit.cost, forfeit_id: f.forfeit.id ,sender_id: f.user_id, sender: User.where(id: f.user_id).first.name, date: f.created_at.strftime("%m/%d/%y"), completed: f.completed , accepted: f.accepted ? true : ( f.accepted.nil? ? "pending" : false) }}
            success.as_json.merge(forfeits: forfeits)
          end

        desc "latest forfeits challenges received on a conversation"
          post "pending" do
            conversation = Conversation.where(id: params[:conversation_id]).first
            success = {:message => "success", error: false}
            forfeit_received = @api_user.forfeits_received(conversation).where(accepted: nil).last
            forfeit_sent = @api_user.challenged_forfeits(conversation).where(accepted: nil).last
            if forfeit_received
              forfeit = {challenged_forfeit_id: forfeit_received.id, forfeit: forfeit_received.forfeit.title, cost: forfeit_received.forfeit.cost, forfeit_id: forfeit_received.forfeit.id ,sender_id: forfeit_received.user_id, sender: User.where(id: forfeit_received.user_id).first.name, date: forfeit_received.created_at.strftime("%m/%d/%y") , accepted: forfeit_received.accepted ? true : ( forfeit_received.accepted.nil? ? "pending" : false) }
            elsif forfeit_sent
              forfeit = {challenged_forfeit_id: forfeit_sent.id, forfeit: forfeit_sent.forfeit.title, cost: forfeit_sent.forfeit.cost, forfeit_id: forfeit_sent.forfeit.id ,recipient_id: forfeit_sent.user_id, recipient: User.where(id: forfeit_sent.recipient_id).first.name, date: forfeit_sent.created_at.strftime("%m/%d/%y"), accepted: forfeit_sent.accepted ? true : ( forfeit_sent.accepted.nil? ? "pending" : false) }
            else
              forfeit = {}
            end
            success.as_json.merge(forfeits: forfeit)
          end

        desc "accept foreits"
        post "accept" do
          challenged_forfeit = ChallengedForfeit.where(id: params[:challenged_forfeit_id]).first
          if challenged_forfeit.accepted.nil?
            challenged_forfeit.accepted = true
            if challenged_forfeit.save
              UserMailer.accepted_forfeit(challenged_forfeit.user, challenged_forfeit.forfeit, @api_user).deliver_now if params[:email]
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            {:errors => "you have already accepted it", error: true}
          end

        end

        desc "decline a foreits"
        post "decline" do
          conversation = Conversation.where(id: params[:conversation_id]).first
          challenged_forfeit = ChallengedForfeit.where(id: params[:challenged_forfeit_id]).first
          if challenged_forfeit.accepted.nil?
            forfeit_score = challenged_forfeit.forfeit.cost.to_i
            challenged_forfeit.accepted = false
            if challenged_forfeit.save
              @api_user.update_score(conversation, -50)
              challenged_forfeit.user.update_score(conversation, forfeit_score)
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            {:errors => "you have already declined it", error: true}
          end

        end

        desc "acknowledge foreits"
        post "complete" do
          challenged_forfeit = ChallengedForfeit.where(id: params[:challenged_forfeit_id]).first
          if challenged_forfeit.present? && challenged_forfeit.accepted
            challenged_forfeit.completed = true
            if challenged_forfeit.save
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            if challenged_forfeit.present?
              {:errors => "partner hasn't accepted or declined it", error: true}
            else
              {:errors => "no such forfeit", error: true}
            end
          end

        end

      end


    end
  end
end
