module API
  module V1
    class Questions < Grape::API
      version 'v1' # path-based versioning by default
      format :json
      before do
        error!('Unauthorized. Invalid or expired token.', 200) unless current_user
      end


      resource :questions do

        # desc "test questions"
        # post "test" do
        #   Question.generate_rps_question(params[:game_id], %w[multiple multiple confession multiple undress multiple confession undress multiple multiple])
        # end

        desc "answer a question"
        post "answer" do
          # https://scontent-hkg3-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/12994326_1020424878006390_2274554095490874481_n.jpg?oh=d96b89a9b215ce67f037b896c1592df9&oe=5779E726
          message = Message.where(id: params[:message_id]).last
          unless message
            {errors: "no such message, check message_id", error: true}
          else
            if message.loser == @api_user || (message.question.qtype == "general" if message.question.present?)
              if message.question.qtype == "general"
                @api_user == message.user1 ?  message.update_attributes(user1_read: true) : message.update_attributes(user2_read: true)
              else
                message.update_attributes(user1_read: true)
                message.update_attributes(user2_read: true)
              end
              conversation = Conversation.set_conversation(params[:conversation_id])
              @api_user == conversation.sender ? other_user = conversation.recipient : other_user = conversation.sender
              question = Question.get_question(params[:question_id])
              # PlayedQuestion.first_or_create()
              played_game = PlayedGame.where(user_id: @api_user, game_id: question.game.id, conversation_id: conversation.id ).first
              played_question = PlayedQuestion.where(message_id: message.id, user_id: @api_user.id, question_id: question.id, played_game_id: played_game.id )
              unless played_question.present?
                played_question = PlayedQuestion.create(message_id: message.id, user_id: @api_user.id, question_id: question.id, played_game_id: played_game.id )
                played_question.save
                answer = Answer.get_answer(params[:answer_id])
                PlayedAnswer.create(played_question_id: played_question.id, answer_id: answer.id, message_id: message.id, user_id: @api_user.id)
                # PlayedAnswer.first_or_create()
                if question.qtype == "general"
                  if question.answers.include?(answer)
                    if answer.correct
                      @api_user.update_score(conversation , 10)

                      score = {user_score: @api_user.conversation_score(conversation.id), partner_score: other_user.conversation_score(conversation.id)}
                      {message: "success" , result: "correct", error: false}.merge(score)
                    else
                      score = {user_score: @api_user.conversation_score(conversation.id), partner_score: other_user.conversation_score(conversation.id)}
                      {message: "success" , result: "incorrect", error: false }.merge(score)
                    end
                  else
                    {errors: "unassociated answer", error: true}
                  end
                else

                  # unless Message.where(question: question , loser: @api_user , user2: @api_user, user1: other_user, winner: other_user, rps: false , answered: true, answer: answer ).last.present?
                    msg = conversation.messages.build(question: question , loser: @api_user , user2: @api_user, user1: other_user, winner: other_user, rps: false , answered: true, answer: answer )
                    @api_user == message.user1 ? msg.user1_read = true : msg.user2_read = true
                    if msg.save
                      score = {user_score: @api_user.conversation_score(conversation.id), partner_score: other_user.conversation_score(conversation.id)}
                      {message: "success", error: false }.merge(score)
                    else
                      {errors: "something went wrong", error: true}
                    end
                  # else
                  #   {errors: "you have already answered it", error: true}
                  # end

                end
              else
                {errors: "you have already answered it", error: true}
              end
            else
              {errors: "you are not allowed to answer this question", error: true}
            end
          end



        end



      end
    end
  end
end
