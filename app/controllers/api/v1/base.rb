module API
  module V1
    class Base < Grape::API
      mount API::V1::Users
      mount API::V1::Conversations
      mount API::V1::Games
      mount API::V1::Questions
      mount API::V1::Messages
      mount API::V1::Forfeits
    end
  end
end