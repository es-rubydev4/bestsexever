module API
  module V1
    class Conversations < Grape::API
      version 'v1' # path-based versioning by default
      format :json
      before do
        error!('Unauthorized. Invalid or expired token.', 200) unless current_user
      end

      resource :conversations do
        # desc "rock paper scissors"
        # post "/rps" do
        #   if params[:conversation_id] && params[:rps]
        #     @conversation = Conversation.set_conversation(params[:conversation_id])
        #     {:error => "no such conversatipn"} unless @conversation
        #     @api_user.plays.first_or_create.update(conversation_id: @conversation.id, rps: params[:rps], user_id: @api_user.id, read: false)
        #     recipient = Play.where(conversation_id: @conversation.id, user_id: @conversation.recipient.id, read:false ).last
        #     sender = Play.where(conversation_id: @conversation.id, user_id: @conversation.sender.id, read: false ).last
        #     Play.play_rps(@conversation, recipient, sender)
        #     {:message => "success"}
        #   else
        #     {:error => "insufficient parameters"}
        #   end
        #
        # end

        # desc "sned conversation invitation"
        # post "invite" do
        #   user = User.set_user(params[:recipient_id])
        #   if user
        #     if user == @api_user
        #       {:errors => "Can't invite yourself", error: true }
        #     else
        #       @invitation = Invitation.create(sender_id: @api_user.id, recipient_id: user.id )
        #       if @invitation.save
        #         user.as_json(:only => [:email, :name ]).merge({:message => "success", user_id: user.id, error: false})
        #       else
        #         {:errors => @invitation.errors.full_messages.to_sentence, error: true}
        #       end
        #     end
        #
        #   else
        #     {:errors => "no such recipient", error: true }
        #   end
        #
        # end

        # desc "conversation invitation acknowledgment"
        # get "host" do
        #   if @api_user.invites
        #     @api_user.invites.as_json(:only => [:accepted, :sender_id, :recipient_id, :conversation_id ]).merge({:message => "success" }) || {}
        #   else
        #     {}
        #   end
        # end
        #
        #
        # desc "conversation invitations for user"
        # get "join" do
        #   if @api_user.invites
        #     user = User.set_user(@api_user.invites.sender_id)
        #     @api_user.invites.as_json(:only => [:accepted, :sender_id, :recipient_id ]).merge({:message => "success", :sender => user.name, :email => user.email, gender: user.gender }) || {}
        #   else
        #     {}
        #   end
        #
        # end

        # desc "create"
        # post "/create"  do
        #   if params[:sender_id].present? && params[:recipient_id].present? && params[:sender_id] != params[:recipient_id]
        #     if User.set_user(params[:sender_id]) && User.set_user(params[:recipient_id])
        #       @conversation = Conversation.between(params[:sender_id],params[:recipient_id]).first_or_create(sender_id:params[:sender_id], recipient_id:params[:recipient_id])
        #       if @conversation.save
        #         @api_user.clear_invites
        #         invitation = Invitation.where(sender_id: @api_user.id, recipient_id: params[:sender_id], conversation_id: @conversation.id, accepted: true )
        #         unless invitation.present?
        #           invitation = Invitation.create(sender_id: @api_user.id, recipient_id: params[:sender_id], conversation_id: @conversation.id, accepted: true )
        #           @conversation.as_json(:only => [:recipient_id, :sender_id ]).merge({:message => "success", conversation_id: @conversation.id}) if invitation.save
        #         end
        #         @conversation.as_json(:only => [:recipient_id, :sender_id ]).merge({:message => "success", conversation_id: @conversation.id})
        #       else
        #         {:error => @conversation.errors.full_messages.to_sentence}
        #       end
        #     else
        #       {:error => "no such user" }
        #     end
        #   else
        #     {:error => "insufficeint params error" }
        #   end
        # end


        desc "show"
        get "/show" do
          conversation = Conversation.set_conversation(params[:id])
          if conversation.present?
            # @api_user.clear_invites
            if @api_user == conversation.sender || @api_user == conversation.recipient
              conversation.save
              conversation ? @api_user.show_detail(conversation) : {:errors => "no such conversation" , error: true}
            else
              {:errors => "conversation not associated with you" , error: true}
            end
          else
            {:errors => "no such conversation" , error: true}
          end

        end

        desc "connection check api"
        post "set" do
          conversation = Conversation.set_conversation(params[:id])
          if conversation.present?
            if @api_user == conversation.sender || @api_user == conversation.recipient
              if @api_user == conversation.sender
                conversation.sender_read = true
              elsif @api_user == conversation.recipient
                conversation.recipient_read = true
              end
              if conversation.save
                {:message => "success" , error: false}
              end
            else
              {:errors => "conversation not associated with you" , error: true}
            end
          else
            {:errors => "no such conversation" , error: true}
          end

        end




      end

    end
  end
end