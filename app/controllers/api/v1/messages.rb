module API
  module V1
    class Messages < Grape::API
      version 'v1' # path-based versioning by default
      format :json
      before do
        error!('Unauthorized. Invalid or expired token.', 200) unless current_user
      end


      resource :messages do

        desc "acknowledge the message"
        post "ack" do
          message = Message.where(id: params[:message_id]).last
          if @api_user == message.winner
            if @api_user == message.user1
              if message.update_attributes(user1_read: true)
                {:message => "success", error: false}
              else
                {:errors => "failed", error: true}
              end
            else
              if message.update_attributes(user2_read: true)
                {:message => "success", error: false}
              else
                {:errors => "failed", error: true}
              end
            end
          elsif message.question.qtype == "finish"
            if message.update_attributes(user1_read: true, user2_read: true)
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            {:errors => "you are not supposed to ack this", error: true}
          end

        end

      end


    end
  end
end
