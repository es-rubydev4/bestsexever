module API
  module V1
    class Games < Grape::API
      version 'v1' # path-based versioning by default
      format :json


      resource :games do

        desc "games list"
        get "list" do
          games = Game.all.as_json(:only => [:id, :title, :free])
          {:message => "success", error: false}.as_json.merge(games: games.sort_by { |k| k["title"].to_i })
        end

        desc "all questions"
        get "questions" do
          if params[:game_id]
            game = Game.find_by_id(params[:game_id])
            game ? game.question.map { |x| {question: x , answers: x.answers } } : {error: true, errors: "no such game_id"}
          else
            Question.all.map { |x| {question: x , answers: x.answers } }
          end
        end

        get "users_game" do
          authenticate!
          games = Game.all.map { |g| {game_id: g.id, title: g.title, can_play: g.free ? true : @api_user.bought_games.any? { |bg| bg[:game_id] == g.id} } }
          {:message => "success", error: false}.as_json.merge(games: games.sort_by { |k| k[:title].to_i })
        end

        desc "buy a game"
        post "buy" do
          authenticate!
          if params[:game_ids]
            revenue = Revenue.first_or_create
            if params[:game_ids].count == 1
              revenue.amount = revenue.amount + 0.99
            elsif params[:game_ids].count == 5
              revenue.amount = revenue.amount + 1.99
            elsif params[:game_ids].count == 11
              revenue.amount = revenue.amount + 2.99
            end
            revenue.save
            params[:game_ids].each do |g|
              game = Game.set_game(g)
              if game.present?
                unless @api_user.bought_games.any? { |bg| bg[:game_id] == game.id}
                  bought_game = @api_user.bought_games.create(game_id: game.id)
                  bought_game.save
                end
              end
            end
            message = {:message => "success", error: false}
          elsif params[:game_id]
            game = Game.set_game(params[:game_id])
            if game.present?
              unless @api_user.bought_games.any? { |bg| bg[:game_id] == game.id}
                bought_game = @api_user.bought_games.create(game_id: game.id)
                if bought_game.save
                  message = {:message => "success", error: false}
                else
                  message = {:errors => "failed buying", error: true}
                end
              else
                message = {:message => "success", error: false}
              end
            end
          end
          games = Game.all.map { |g| {game_id: g.id, title: g.title, can_play: g.free ? true : @api_user.bought_games.any? { |bg| bg[:game_id] == g.id} } }
          message.as_json.merge(games: games.sort_by { |k| k[:title].to_i })
        end

        desc "game select"
        post "select" do
          authenticate!
          game = Game.set_game(params[:game_id])
          conversation = Conversation.set_conversation(params[:conversation_id])
          Conversation.update_count(conversation)
          partner = User.set_user(params[:partner_id])
          # unless @api_user.general_question_for(conversation, game) || partner.general_question_for(conversation, game)

            if game && conversation && partner
              if game.free || @api_user.bought_games.any? { |bg| bg[:game_id] == game.id}
                @api_user.played_games.where(game_id: game.id, conversation_id: conversation.id).first_or_create
                partner.played_games.where(game_id: game.id, conversation_id: conversation.id).first_or_create
                @api_user.clear_general_questions(conversation) if @api_user.general_questions(conversation)
                @api_user.clear_rps_questions(conversation)
                partner.clear_rps_questions(conversation)
                Message.reset(conversation)

                # if !Rails.env.production?
                #   conversation.recipient.generate_rps_question(game.id, conversation , %w[multiple confession foreplay sex finish])
                #   conversation.sender.generate_rps_question(game.id, conversation , %w[multiple confession foreplay sex finish])
                # else
                  conversation.sender.male?  ? conversation.sender.generate_rps_question_male(game.id, conversation) : conversation.sender.generate_rps_question_female(game.id, conversation)
                  conversation.recipient.male? ? conversation.recipient.generate_rps_question_male(game.id, conversation) : conversation.recipient.generate_rps_question_female(game.id, conversation)
                  # conversation.sender.male?  ? conversation.sender.male_generate_rps_question(game.id, conversation ) : conversation.sender.female_generate_rps_question(game.id, conversation )
                  # conversation.recipient.male? ? conversation.recipient.male_generate_rps_question(game.id, conversation ) : conversation.recipient.female_generate_rps_question(game.id, conversation )
                # end

                # unless @api_user.general_question_for(conversation, game)
                # questions = Question.generate_general_question(game.id)
                # temp = false
                # questions.each do |q|
                #   msg = conversation.messages.build(question: q , user1: @api_user , user2: partner , rps: false )
                #   msg.save ? temp = true : temp = false
                # end
                # if temp
                  {:message => "success", conversation_id: conversation.id, error: false}
                # else
                #   {:errors => "failed", error: true}
                # end
                # else
                #   {:message => "success", conversation_id: conversation.id, error: false}
                # end

              else
                {:errors => "buy the game", error: true}
              end
            else
              {:errors => "no such game, conversation or user", error: true}
            end

          # else
          #   {:message => "success", conversation_id: conversation.id, error: false}
          # end

        end

        desc "win api"
        post "won" do
          authenticate!
          if params[:conversation_id]
            conversation = Conversation.set_conversation(params[:conversation_id])
            @api_user.update_score(conversation, 10)
            {:message => "success", error: false}
          else
            {:errors => "insufficient parameters", error: true}
          end
        end

        desc "lost api"
        post "lost" do
          authenticate!
          if params[:conversation_id]
            conversation = Conversation.set_conversation(params[:conversation_id])
            @api_user == conversation.sender ? winner = conversation.recipient : winner = conversation.sender
            @api_user.update_score(conversation, -5)
            question = RpsQuestion.where(user: @api_user, conversation: conversation).first
            msg1 = conversation.messages.build(question: question.question , winner: winner , loser: @api_user, rps: true, user1: winner, user2: @api_user )
            question.destroy
            if msg1.save
              {:message => "success", error: false}
            else
              {:errors => "failed", error: true}
            end
          else
            {:errors => "insufficient parameters", error: true}
          end
        end

        desc "single api for score update for winner and setting up question to loser"
        post "result" do
          authenticate!
          conversation = Conversation.set_conversation(params[:conversation_id])
          msg = Message.where(conversation_id: conversation.id ).last
          if msg.nil? || msg.user2_read && msg.user1_read
            if @api_user == conversation.sender || @api_user == conversation.recipient
              unless @api_user.general_questions(conversation)
                if params[:conversation_id]
                  loser = conversation.sender == @api_user ? conversation.recipient : conversation.sender
                  @api_user.update_score(conversation, 10)
                  loser.update_score(conversation, -5)
                  question = RpsQuestion.where(user: loser, conversation: conversation).first
                  @api_user == conversation.sender ? other_user = conversation.recipient : other_user = conversation.sender
                  score = {user_score: @api_user.conversation_score(conversation.id), partner_score: other_user.conversation_score(conversation.id)}

                  unless question.nil?
                    msg1 = conversation.messages.build(question: question.question , winner: @api_user , loser: loser, rps: true, user1: @api_user, user2: loser )
                    question.destroy # deletes this question form set of user's rps questions
                    if msg1.save
                      {:message => "success", error: false}.merge(score)
                    else
                      {:errors => "failed", error: true}.merge(score)
                    end
                  else
                    {:errors => "game over !", error: true}.merge(score)
                  end
                else
                  {:errors => "insufficient parameters", error: true}
                end
              else
                {:errors => "answer general questions first", error: true}
              end
            else
              {:errors => "conversation not associated with you" , error: true}
            end
          else
            {:message => "success", :flag => "repeated", error: false}
          end


        end


      end


    end
  end
end
