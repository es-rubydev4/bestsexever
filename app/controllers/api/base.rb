module API
  class Base < Grape::API
    helpers do
      def authenticate!
        error!('Unauthorized. Invalid or expired token.', 200) unless current_user
      end

      def current_user
        # find token. Check if valid.
        user = User.where(api_key: params[:api_key]).first
        if user
          @api_user = user
          user.update( last_sign_in_at: Time.now)
          user.update( last_sign_in_ip: request.ip )
        else
          false
        end
      end

      def user_params
        params.permit(:name, :email, :password, :gender)
      end

      def conversation_params
        params.permit(:sender_id, :recipient_id)
      end

      def interlocutor(conversation)
        current_user == conversation.recipient ? conversation.sender : conversation.recipient
      end

    end



    mount API::V1::Base


  end
end